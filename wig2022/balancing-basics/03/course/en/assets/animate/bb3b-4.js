(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"bb3b_4_atlas_", frames: [[544,81,45,79],[0,0,542,900],[544,0,45,79]]}
];


// symbols:



(lib.correct = function() {
	this.spriteSheet = ss["bb3b_4_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.cutaway = function() {
	this.spriteSheet = ss["bb3b_4_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.incorrect = function() {
	this.spriteSheet = ss["bb3b_4_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.incorrectWord = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#ED1C24").s().p("AgbBtIAAivIhEAAIAAgqIC/AAIAAAqIhEAAIAACvg");
	this.shape.setTransform(89.6,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#ED1C24").s().p("AgbBqQgUgHgPgOQgPgOgIgWQgIgVAAgcQAAgZAIgVQAHgVAPgPQAOgOAVgIQAVgIAYAAQAOAAALABIAVAEIARAHIAOAGIAAA1IgGAAIgKgIIgPgKIgRgIQgJgDgLAAQgMAAgLAEQgJAEgJAIQgJAJgFANQgGAOAAATQAAAUAGAOQAGANAJAIQAJAIAKAEQALADAKAAQALAAAKgDQAKgDAIgFIANgJIALgIIAFAAIAAA0IgPAHIgQAFIgUAFQgJABgRAAQgXAAgVgHg");
	this.shape_1.setTransform(68.6,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#ED1C24").s().p("AhOBtIAAjZICdAAIAAAqIhlAAIAAAlIBeAAIAAAqIheAAIAAA2IBlAAIAAAqg");
	this.shape_2.setTransform(47.8,-0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#ED1C24").s().p("AAkBtIg6hQIgaAAIAABQIg4AAIAAjZIBeAAQASAAAPACQANACANAIQAMAHAHAMQAHALABASQAAAYgMAPQgLAOgVAKIBJBegAgwgJIARAAQAOAAAIgCQAJgBAGgEQAFgFADgGQADgFAAgJQAAgIgDgHQgEgFgIgEQgFgCgHAAIgSgBIgUAAg");
	this.shape_3.setTransform(27,-0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ED1C24").s().p("AAkBtIg6hQIgaAAIAABQIg4AAIAAjZIBeAAQATAAANACQAOACAMAIQANAHAHAMQAIALgBASQAAAYgLAPQgLAOgVAKIBJBegAgwgJIARAAQANAAAKgCQAIgBAGgEQAGgFADgGQACgFAAgJQAAgIgDgHQgEgFgIgEQgFgCgHAAIgSgBIgUAAg");
	this.shape_4.setTransform(3.6,-0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#ED1C24").s().p("AhSBTQgegfAAg0QAAg0AegeQAfgeAzAAQA0AAAfAeQAdAeAAA0QAAA0gdAfQgfAeg0AAQgzAAgfgegAgThDQgJADgJAJQgHAIgFAPQgEAOAAASQAAATAEAOQAFAOAHAJQAHAIAKAEQAKAEAKABQALgBAKgEQAKgEAHgJQAIgKAEgNQAEgOAAgSQAAgTgFgNQgEgPgHgIQgIgJgJgDQgKgEgLgBQgKABgJAEg");
	this.shape_5.setTransform(-22.5,-0.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#ED1C24").s().p("AgbBqQgUgHgPgOQgPgOgIgWQgIgVAAgcQAAgZAIgVQAHgVAPgPQAOgOAVgIQAVgIAYAAQAOAAALABIAVAEIARAHIAOAGIAAA1IgGAAIgKgIIgPgKIgRgIQgJgDgLAAQgMAAgLAEQgJAEgJAIQgJAJgFANQgGAOAAATQAAAUAGAOQAGANAJAIQAJAIAKAEQALADAKAAQALAAAKgDQAKgDAIgFIANgJIALgIIAFAAIAAA0IgPAHIgQAFIgUAFQgJABgRAAQgXAAgVgHg");
	this.shape_6.setTransform(-46,-0.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#ED1C24").s().p("AAtBtIhciVIAACVIgzAAIAAjZIBDAAIBPB8IAAh8IAzAAIAADZg");
	this.shape_7.setTransform(-69.6,-0.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#ED1C24").s().p("Ag/BtIAAgnIAkAAIAAiLIgkAAIAAgnIB/AAIAAAnIgkAAIAACLIAkAAIAAAng");
	this.shape_8.setTransform(-90.5,-0.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(255,255,255,0.749)").s().p("EgnUAFQIAAqfMBOpAAAIAAKfg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-251.7,-33.6,503.4,67.2);


(lib.incorrectLines = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#ED1C24").ss(7,1,1).p("AAAo8IAAj7AABIuIAAEK");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.6,-85.8,7.3,171.8);


(lib.incorrect_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.incorrect();
	this.instance.parent = this;
	this.instance.setTransform(-22.5,-39.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-39.5,45,79);


(lib.cutaway_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cutaway();
	this.instance.parent = this;
	this.instance.setTransform(-271,-450);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-271,-450,542,900);


(lib.correctWord = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#39894B").s().p("AgbBtIAAivIhEAAIAAgqIC/AAIAAAqIhEAAIAACvg");
	this.shape.setTransform(68.7,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#39894B").s().p("AgbBqQgUgHgPgOQgPgOgIgWQgIgVAAgcQAAgZAIgVQAHgVAPgPQAOgOAVgIQAVgIAYAAQAOAAALABIAVAEIARAHIAOAGIAAA1IgGAAIgKgIIgPgKIgRgIQgJgDgLAAQgMAAgLAEQgJAEgJAIQgJAJgFANQgGAOAAATQAAAUAGAOQAGANAJAIQAJAIAKAEQALADAKAAQALAAAKgDQAKgDAIgFIANgJIALgIIAFAAIAAA0IgPAHIgQAFIgUAFQgJABgRAAQgXAAgVgHg");
	this.shape_1.setTransform(47.7,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#39894B").s().p("AhOBtIAAjZICdAAIAAAqIhlAAIAAAlIBeAAIAAAqIheAAIAAA2IBlAAIAAAqg");
	this.shape_2.setTransform(26.9,-0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#39894B").s().p("AAkBtIg6hQIgaAAIAABQIg4AAIAAjZIBeAAQATAAAOACQAOACAMAIQAMAHAHAMQAIALAAASQAAAYgMAPQgLAOgVAKIBJBegAgwgJIARAAQAOAAAIgCQAJgBAGgEQAGgFADgGQACgFAAgJQAAgIgDgHQgDgFgJgEQgEgCgIAAIgSgBIgUAAg");
	this.shape_3.setTransform(6.1,-0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#39894B").s().p("AAkBtIg6hQIgaAAIAABQIg4AAIAAjZIBeAAQASAAAOACQAOACAMAIQANAHAHAMQAHALAAASQAAAYgLAPQgLAOgVAKIBJBegAgwgJIARAAQAOAAAJgCQAIgBAGgEQAFgFADgGQADgFAAgJQAAgIgDgHQgDgFgJgEQgEgCgIAAIgSgBIgUAAg");
	this.shape_4.setTransform(-17.3,-0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#39894B").s().p("AhSBTQgdgfAAg0QAAg0AdgeQAegeA0AAQA1AAAdAeQAfAeAAA0QAAA0gfAfQgdAeg1AAQg0AAgegegAgThDQgJADgJAJQgGAIgFAPQgFAOAAASQAAATAFAOQAEAOAHAJQAIAIAJAEQAKAEAKABQALgBAKgEQAJgEAIgJQAIgKAEgNQAEgOAAgSQAAgTgEgNQgFgPgIgIQgHgJgJgDQgKgEgLgBQgKABgJAEg");
	this.shape_5.setTransform(-43.4,-0.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#39894B").s().p("AgbBqQgUgHgPgOQgPgOgIgWQgIgVAAgcQAAgZAIgVQAHgVAPgPQAOgOAVgIQAVgIAYAAQAOAAALABIAVAEIARAHIAOAGIAAA1IgGAAIgKgIIgPgKIgRgIQgJgDgLAAQgMAAgLAEQgJAEgJAIQgJAJgFANQgGAOAAATQAAAUAGAOQAGANAJAIQAJAIAKAEQALADAKAAQALAAAKgDQAKgDAIgFIANgJIALgIIAFAAIAAA0IgPAHIgQAFIgUAFQgJABgRAAQgXAAgVgHg");
	this.shape_6.setTransform(-66.9,-0.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.749)").s().p("EgnUAFQIAAqfMBOpAAAIAAKfg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-251.7,-33.6,503.4,67.2);


(lib.correctLines = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#39894B").ss(3,1,1).p("AAvkTIhbAAIAAhuAAtEUIhbAAIAABu");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.1,-40.1,12.3,80.2);


(lib.correct_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.correct();
	this.instance.parent = this;
	this.instance.setTransform(-22.5,-39.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-39.5,45,79);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.incorrectLines("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(260.2,226.7,1.208,0.947,0,0,0,0,0.1);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(168).to({_off:false},0).to({alpha:1},24).wait(28));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.incorrect_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(280.9,225,1.366,1.366);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(120).to({_off:false},0).wait(24).to({regX:0.1,regY:0.2,scaleX:1.94,scaleY:1.94,x:246.4,y:226.5},0).to({alpha:1},24).wait(52));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.correctLines("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(268.6,227.3,1.737,2.005,0,0,0,0.1,0.1);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(48).to({_off:false},0).to({alpha:1},24).wait(24).to({startPosition:0},0).to({alpha:0},24).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.correct_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-48.3,227,1.815,1.815,0,0,0,0.1,0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(24).to({_off:false},0).to({x:235.2},24).wait(48).to({startPosition:0},0).to({alpha:0},24).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cutaway_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(323.2,280,0.444,0.444,0,0,0,74.6,123.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(24).to({startPosition:0},0).to({regX:74.8,regY:123.9,scaleX:1.42,scaleY:1.42,x:362.3,y:400.7},24).wait(172));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_incorrect = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// incorrect
	this.instance = new lib.incorrectWord("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(290,409.4,1.208,1.208);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(168).to({_off:false},0).to({alpha:1},24).wait(28));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_correct = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// correct
	this.instance = new lib.correctWord("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(290,409.4,1.208,1.208);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(48).to({_off:false},0).to({alpha:1},24).wait(24).to({startPosition:0},0).to({alpha:0},24).wait(100));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.bb3b4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_219 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(219).call(this.frame_219).wait(1));

	// incorrect_obj_
	this.incorrect = new lib.Scene_1_incorrect();
	this.incorrect.name = "incorrect";
	this.incorrect.parent = this;
	this.incorrect.depth = 0;
	this.incorrect.isAttachedToCamera = 0
	this.incorrect.isAttachedToMask = 0
	this.incorrect.layerDepth = 0
	this.incorrect.layerIndex = 0
	this.incorrect.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.incorrect).wait(220));

	// correct_obj_
	this.correct = new lib.Scene_1_correct();
	this.correct.name = "correct";
	this.correct.parent = this;
	this.correct.depth = 0;
	this.correct.isAttachedToCamera = 0
	this.correct.isAttachedToMask = 0
	this.correct.layerDepth = 0
	this.correct.layerIndex = 1
	this.correct.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.correct).wait(220));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(220));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 3
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(220));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 4
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(120).to({_off:true},24).wait(76));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 5
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(220));

	// Layer_1_obj_
	this.Layer_1 = new lib.Scene_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(290,225,1,1,0,0,0,290,225);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 6
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(220));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(459.6,250.1,240.8,399.8);
// library properties:
lib.properties = {
	id: '77FD907C44E096408A2571A186318571',
	width: 580,
	height: 450,
	fps: 24,
	color: "#DDDDDD",
	opacity: 1.00,
	manifest: [
		{src:"images/bb3b_4_atlas_.png", id:"bb3b_4_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['77FD907C44E096408A2571A186318571'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;