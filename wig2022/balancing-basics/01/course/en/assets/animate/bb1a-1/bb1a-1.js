(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.ignorePause = false;
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.Tween2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ABAigQgggNgjgDQgXgBgSAAIgOAAIgBBEQABBPAOA3QALAsAPA8QAMAmAQAHQAQAGASgCQAIgBAGgCQgFkVALg6g");
	this.shape.setTransform(3.9,-40.1,1,1,0,0,0,0.2,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ACoggQhrA+hMA1QhMA0gIAFQgcAWgPAdIgYArQgBgQABgbQADg2APg3QAPg2AWiMIAUiCIAtAXQA1AeAxAlQAwAkAlAuQASAWAJAQg");
	this.shape_1.setTransform(27.9875,-30.9936);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ACnAMQADghgJgjQgFgWgGgSIgFgNQgbAHgmANQhLAZgvAfQgmAYg1AgQghAXgBASQgBASAIAQQADAIAEAEICLguQCRgvAkgFg");
	this.shape_2.setTransform(39.6225,-8.7545);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AB+CdQhdhShJg5QhHg3gJgGQgegUgggGIgwgJIApgNQA0gPA4gCQBagEDsgrIgHAyQgMA+gUA4QgUA5ggAxQgQAZgMANg");
	this.shape_3.setTransform(33.5761,16.4673);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAqCYQAggJAggTQATgMAPgKIAKgJQgPgYgXggQgvg+gsgkQg9g1gVgQQgggZgRAFQgRAFgOAMQgGAGgDAFIBXB1QBZB8AQAhg");
	this.shape_4.setTransform(20.2024,36.2826);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AimDFQAxhyAfhWQAghZACgHQAKgigEghIgHgwIAaAjQAdAtAUA1QAfBTByDUQgTAEgfAEQg+AIg8gBQg8gBg5gQg");
	this.shape_5.setTransform(0.25,34.3,1,1,0,0,0,0.1,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("Ah6BbIANASQAPAUASAPQAbAXAQAKQATgWAWggQAthBAVg1QAghJAIgZQANgmgKgQQgJgOgQgJIgNgGIhUB3QhaB7gbAZg");
	this.shape_6.setTransform(-29.336,30.141);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AjaiUIBKAGQBVAGA6ACQBZADAMAAQAjgCAegOIAsgVIgZAjQgiAqgsAkQhHA3ilCtQgKgQgNgdQgbg4gSg6QgRg5gDg6g");
	this.shape_7.setTransform(-34.9028,16.0738);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AiDhUIgMARQgPAVgJAVQgNAhgEASIBAAWQBLAYA4ADQAuADA9AFQAoABAMgPQALgNAEgSIABgOQkJhRg0gbg");
	this.shape_8.setTransform(-37.2791,-18.7449);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AA9jJIARBHQAVBTAQA3QAaBbADAHQAMAhAWAYIAiAkQgQgEgZgJQgzgUgvgfQgvgfh+hBIh2g6QANgOAXgVQAugrAxgjQAygiA2gVQAbgLARgDg");
	this.shape_9.setTransform(-25.0164,-33.2326);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAMAkQgOAFgOgHQgOgHgFgPQgFgOAIgOQAHgOAOgFQAOgFAOAIQAOAHAFAPQAFANgHAOQgHAOgPAFg");
	this.shape_10.setTransform(15.4292,0.038);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AgdAWQgKgMADgPQACgPANgJQAMgKAPADQAPACAKANQAJANgDAOQgCAPgNAKQgNAJgOgDQgPgCgJgNg");
	this.shape_11.setTransform(4.4761,14.8761);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AgdgVQAJgNAPgCQAPgDAMAKQANAJACAPQADAPgJAMQgKANgPACQgOADgNgJQgNgKgCgPQgDgOAKgNg");
	this.shape_12.setTransform(-13.0239,9.0261);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAMgjQAOAFAIAOQAHAOgFAOQgFAPgOAHQgOAHgOgFQgOgFgHgOQgIgOAFgNQAFgPAOgHQAOgIAOAFg");
	this.shape_13.setTransform(-12.8708,-9.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAmAAQAAAQgLALQgMALgPAAQgPAAgLgLQgLgLAAgQQAAgOALgMQALgLAPAAQAQAAALALQALAMAAAOg");
	this.shape_14.setTransform(4.725,-14.975);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ABWAAQAAAjgZAZQgZAZgkAAQgiAAgZgZQgZgZAAgjQAAgiAZgZQAZgZAiAAQAkAAAZAZQAZAZAAAig");
	this.shape_15.setTransform(-0.25,-0.075);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AJ1AAQAAEEi4C5Qi4C4kFAAQkDAAi5i4Qi4i5AAkEQAAkEC4i4QC5i4EDAAQEFAAC4C4QC4C4AAEEg");
	this.shape_16.setTransform(-0.275,-0.075);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAAuCQC3AACnBHQChBFB8B8QB9B8BECiQBGCmAAC2QAAC3hGCnQhEChh9B9Qh8B8ihBEQinBHi3AAQi2AAimhHQiihFh8h7Qh8h9hFihQhGinAAi3QAAi2BGimQBFiiB8h8QB8h8CihFQCnhHC1AAg");
	this.shape_17.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-90.8,-90.8,181.7,181.7);


(lib.Tween1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ABAigQgggNgjgDQgXgBgSAAIgOAAIgBBEQABBPAOA3QALAsAPA8QAMAmAQAHQAQAGASgCQAIgBAGgCQgFkVALg6g");
	this.shape.setTransform(3.9,-40.1,1,1,0,0,0,0.2,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ACoggQhrA+hMA1QhMA0gIAFQgcAWgPAdIgYArQgBgQABgbQADg2APg3QAPg2AWiMIAUiCIAtAXQA1AeAxAlQAwAkAlAuQASAWAJAQg");
	this.shape_1.setTransform(27.9875,-30.9936);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ACnAMQADghgJgjQgFgWgGgSIgFgNQgbAHgmANQhLAZgvAfQgmAYg1AgQghAXgBASQgBASAIAQQADAIAEAEICLguQCRgvAkgFg");
	this.shape_2.setTransform(39.6225,-8.7545);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AB+CdQhdhShJg5QhHg3gJgGQgegUgggGIgwgJIApgNQA0gPA4gCQBagEDsgrIgHAyQgMA+gUA4QgUA5ggAxQgQAZgMANg");
	this.shape_3.setTransform(33.5761,16.4673);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAqCYQAggJAggTQATgMAPgKIAKgJQgPgYgXggQgvg+gsgkQg9g1gVgQQgggZgRAFQgRAFgOAMQgGAGgDAFIBXB1QBZB8AQAhg");
	this.shape_4.setTransform(20.2024,36.2826);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AimDFQAxhyAfhWQAghZACgHQAKgigEghIgHgwIAaAjQAdAtAUA1QAfBTByDUQgTAEgfAEQg+AIg8gBQg8gBg5gQg");
	this.shape_5.setTransform(0.25,34.3,1,1,0,0,0,0.1,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("Ah6BbIANASQAPAUASAPQAbAXAQAKQATgWAWggQAthBAVg1QAghJAIgZQANgmgKgQQgJgOgQgJIgNgGIhUB3QhaB7gbAZg");
	this.shape_6.setTransform(-29.336,30.141);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AjaiUIBKAGQBVAGA6ACQBZADAMAAQAjgCAegOIAsgVIgZAjQgiAqgsAkQhHA3ilCtQgKgQgNgdQgbg4gSg6QgRg5gDg6g");
	this.shape_7.setTransform(-34.9028,16.0738);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AiDhUIgMARQgPAVgJAVQgNAhgEASIBAAWQBLAYA4ADQAuADA9AFQAoABAMgPQALgNAEgSIABgOQkJhRg0gbg");
	this.shape_8.setTransform(-37.2791,-18.7449);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AA9jJIARBHQAVBTAQA3QAaBbADAHQAMAhAWAYIAiAkQgQgEgZgJQgzgUgvgfQgvgfh+hBIh2g6QANgOAXgVQAugrAxgjQAygiA2gVQAbgLARgDg");
	this.shape_9.setTransform(-25.0164,-33.2326);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAMAkQgOAFgOgHQgOgHgFgPQgFgOAIgOQAHgOAOgFQAOgFAOAIQAOAHAFAPQAFANgHAOQgHAOgPAFg");
	this.shape_10.setTransform(15.4292,0.038);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AgdAWQgKgMADgPQACgPANgJQAMgKAPADQAPACAKANQAJANgDAOQgCAPgNAKQgNAJgOgDQgPgCgJgNg");
	this.shape_11.setTransform(4.4761,14.8761);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AgdgVQAJgNAPgCQAPgDAMAKQANAJACAPQADAPgJAMQgKANgPACQgOADgNgJQgNgKgCgPQgDgOAKgNg");
	this.shape_12.setTransform(-13.0239,9.0261);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAMgjQAOAFAIAOQAHAOgFAOQgFAPgOAHQgOAHgOgFQgOgFgHgOQgIgOAFgNQAFgPAOgHQAOgIAOAFg");
	this.shape_13.setTransform(-12.8708,-9.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAmAAQAAAQgLALQgMALgPAAQgPAAgLgLQgLgLAAgQQAAgOALgMQALgLAPAAQAQAAALALQALAMAAAOg");
	this.shape_14.setTransform(4.725,-14.975);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ABWAAQAAAjgZAZQgZAZgkAAQgiAAgZgZQgZgZAAgjQAAgiAZgZQAZgZAiAAQAkAAAZAZQAZAZAAAig");
	this.shape_15.setTransform(-0.25,-0.075);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AJ1AAQAAEEi4C5Qi4C4kFAAQkDAAi5i4Qi4i5AAkEQAAkEC4i4QC5i4EDAAQEFAAC4C4QC4C4AAEEg");
	this.shape_16.setTransform(-0.275,-0.075);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAAuCQC3AACnBHQChBFB8B8QB9B8BECiQBGCmAAC2QAAC3hGCnQhEChh9B9Qh8B8ihBEQinBHi3AAQi2AAimhHQiihFh8h7Qh8h9hFihQhGinAAi3QAAi2BGimQBFiiB8h8QB8h8CihFQCnhHC1AAg");
	this.shape_17.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-90.8,-90.8,181.7,181.7);


(lib.bb1carai = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AAAuCQC3AACnBHQChBFB8B8QB9B8BECiQBGCmAAC2QAAC3hGCnQhEChh9B9Qh8B8ihBEQinBHi3AAQi2AAinhHQihhFh8h7Qh9h9hEihQhHinAAi3QAAi2BHimQBEiiB9h8QB8h8ChhFQCnhHC2AAg");
	this.shape.setTransform(935,294.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("Eg3mAKXIgUAAQgGgvgGhMQgLiZAFiRQAGiwBii7QBjjAChiIQCXiADDg+QClg0CmACQCsACCjA7QDJBJCLCTQCSCZBPCnQBGCSAUCmQARCBAKCsQAFBWACA8QBmAbAKgLIAdgcUBHbgAsABKAAAQAxAAAtAFQARABA0AHQAaADANgEQAGgCACgCIAPhOIB7AA");
	this.shape_1.setTransform(486.8719,260.3913);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AhDm6IjyN+IIWgeIBXtpg");
	this.shape_2.setTransform(648.0449,66.9873);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("EAqWAAQQgKgjgoggQgfgYhPgqQhGgki7hDQi3hBijguQk1hZleg2QimgajygVQjYgSiTgEQh+gDjjAGQjSAGiUAJQkeARlAA9QkNA0lAB+Qh9AyixBYQijBRibBWQl6DXh8BgQg/AyhJBPIg8BFIGJgVIASgLIGsggQBtgIGXgiQGOggC1gLQFGgVOVg1QHFgaNugYQGRgLGHgIIAXgHIASgaQAVggASghQAdg0AGgSQAMgjgKgig");
	this.shape_3.setTransform(638.4391,76.3312);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AHrD1QAHACADgFQADgFgBgPQgCgUgMhMQgNhWgLgxQgLgxgVg5QgSgwgJgNQgLgSgYgRQgjgZgpgEQgygGh2ACQh7ABhxAIQhzAJh8ANQh7AOgNAFQgQAIgCALQgDAOAbARQAiAWB7BCQCFBGBDAgQBQAlBfAmQBsAqBcAdQBQAaBTAPQAqAIAgAEg");
	this.shape_4.setTransform(1110.0535,142.9262);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AGUjMQiMAAg7ACQjDAHhfAaQiCAkgcANQguAWgKArQgOA+AAgBIgIAgQgIAjABAMQABAUAOAMIAAAJQgSACgUADQgpAGgMAHQgUALgMAJIAGAKQAMALAcAGQA4AKAMABQAxAFBngCQBPgBB2AAQBWgBAegEQBXgJAyg9QAagfAwhdQAnhJAWgzQARgmAEgQQAFgSgFgEQgFgFgDgCQgFgCgTAAg");
	this.shape_5.setTransform(51.6103,289.0423);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ACHjAQABgKgGgHQgIgJgXgMQgUgLgWgSIgTgPIgCABQgBAEAAAHIAAAOIgdAPIAAAPIgMALQgOANgGAIQgJANgCABIgMADIgQASQgQAVgDARIgUBaQgOBAgEA1QgHBVgFBeIAAAJ");
	this.shape_6.setTransform(21.3821,210.4635);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("AB8BWIhNggQhXgkgpgeQgYgTgIgOQgNgVANgT");
	this.shape_7.setTransform(64.9901,167.5556);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ApVEJQgXACgIgMQgJgNAKgXQASgpAvhPQA7hkAiggQA/g5DyhQQBfggCDgcQCjgkBpgEQCdgFCOAIIgKAPQgSAUgqAeQhEAwlKC5Qh6BFiGBFQiWBNghAIQgtAMgTAEQggAGgPgIQgQgIgSgBQgFAAggABg");
	this.shape_8.setTransform(92.9248,193.9205);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ACSBbIgEgpQgbgTgBgQQgDgSACgJQADgTAPgOQAUgSAEgIQAFgLgOgEQgWgFgOgBQgWgBgTAHQgSAHhPAlIhNAkIgpAOIgHAOg");
	this.shape_9.setTransform(840.9236,12.6648);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AgLgeQg4AChNgBQAGAVAPANQAWAUAjAJQAkAIA3gCQA0gCAegKQAagJAEgLQADgHAGgoQhlAIg4ABg");
	this.shape_10.setTransform(830.178,147.8042);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ABcANQAigCASAAQgZgvgaAAQgqAAgsAEQhAAGgcALQgYAKgRAOQgOAOgEANQCMgQBggHg");
	this.shape_11.setTransform(829.9343,133.5155);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AEKgwQh0gGhWAGQhDAFhqAMQhtALgWAFQgTAEgJAEIgGADIgBASQgBAUABAEQABAGAMAFIBMACQBZACA9gCQBggDDRgSQAIAAAAgmQAAgogLAAg");
	this.shape_12.setTransform(832.725,139.6821);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AgLgeQg3AChOgBQAGAUAPAOQAWAUAkAJQAkAIA2gCQA0gCAfgKQAZgJAFgLQAEgKAEglQhlAIg4ABg");
	this.shape_13.setTransform(580.3145,166.7052);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("ABcANQAigCASAAQgZgvgaAAQgqAAgsAEQhAAGgcALQgYAKgRAOQgOAOgEANQCKgQBigHg");
	this.shape_14.setTransform(580.1341,152.416);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AEKgwQh0gGhWAGQhDAFhqAMQhtALgWAFQgTAEgJAEIgGADIgBASQgBAUABAEQABAGALAFIBNACQBYACA9gCQBhgDDRgSQAIAAAAgmQAAgogLAAg");
	this.shape_15.setTransform(582.925,158.5821);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("ABCunIgwCQQgzCtgNCTQgRC8gCDcQgBDvASCmQAaDpAfC/IACA3IAZB0");
	this.shape_16.setTransform(636.4812,209.2741);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("AAitVIgjBiQgmB9gJCNQgYFmAtGBQAfEPA2FJ");
	this.shape_17.setTransform(353.4324,222.4035);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("EAsxgPzIjYFcQg/Bmi2ETQjQE5hQB4QhtCkh0CmQhkCMg5BLQggAsgRAWQgeAnggAZQhbBEi1AJQj5AOmrARQnyAUlRAJQk0AIzPAWIyRAV");
	this.shape_18.setTransform(619.608,208.1433);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("AmSGCIAwgpQAygrALgKQBQhQATgUQBkhiBGg/QBvhkEgkeIAcge");
	this.shape_19.setTransform(1049.8731,183.5721);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("AP5FZQgQg9gthXQhaiviQh/QigiOjdhFQi4g6i3AEQjHAEixBEQjrBaitDDQhjBug/CWQggBLgMA1");
	this.shape_20.setTransform(935.275,220.3202);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AvpJxQgFgugChDQgDiHAUhmQAfidAuh5QBBipBkhyQEulZG/ALQHnAMEWFIQBjB0A8CLQA+CNANCOQAMB3gCBmQgBA0gEAbIhzAE");
	this.shape_21.setTransform(935.0638,252.3623);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("ADlC9IhdhAQgxghgfgaQgigcgcghQgrgvg4g0QhMhGgwgY");
	this.shape_22.setTransform(1040.1549,103.8323);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AMwEaQiUhFiqhEQjRhSjRhOQk9h3itg3QibgykMhDIguBMIDtA+QElBSETBfQD2BWENBmQDZBRAiAKQA+ATAjABQAfABAfgLQARgHAJgLg");
	this.shape_23.setTransform(955.7121,56.1413);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AEWg1QhEggjUhQQjEhJgsgMQgdgIgWgFIgQgDIghIWII2gOQBBhbAPgXQBbiKh1g3g");
	this.shape_24.setTransform(866.3991,76.3586);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("Egr4AJgIAfhDQArhOA4g3QBHhGBwhOQB9hYDdiFQD2iSDphuQEGh6EPhYQEAhSFZgwQEQgmD0gIQKpgUHTA7QETAiFUBSQERBBDFBBQC2A9CQA9QCoBIAqAqQArAtALAVQAKASAAAnQAABJgoBCQgQAagoA1IglAwQsXAQnPAOQk0AKn1AYQlrARloAUQqqAmoMAlQkKATm1AmQlFAdjYAUQh2ALhPAFg");
	this.shape_25.setTransform(638.45,75.55,1,1,0,0,0,0.2,0);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("AQ/j8IiXDvQjNAMkYAYQowAyl5BEIpbBt");
	this.shape_26.setTransform(229.2669,141.4628);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("AkRDRQHyklDjhtQCDg/DmhbQBzgtBYghQgbgVgXgTIgBABIgcgCQgDAChfAnQhtAskQB/QlLCZiPBTIrHGdIgugdIgoAbIgoACIgOAIQguAAgvAGQgkAFgQAIQgQAJAOAKQAUAPAUAGQAiAJA8gEQC4gNAxgTQAXgJB/hQQCohoA4ghg");
	this.shape_27.setTransform(401.979,72.7002);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("AwTGpQAOg5AehOQA7icBNhrQBgiDCIhoQCRhuCig4QCeg3DYAKQDFAJCIA2QC5BIB0BWQB8BcBjCSQBWB/AiA7QARAdAAAF");
	this.shape_28.setTransform(227.675,226.8321);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#B3B2B3").ss(0.5,0,0,4).p("ADMCIImXkP");
	this.shape_29.setTransform(137.85,206.225);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#B3B2B3").ss(0.8,0,0,4).p("EBFYAWFIBYADIBSgSQCUgfFMhBQFNhACXglQBLgSAJgFIAUgPQAhgfBHhIQBHhJgBhNQAAgYgIgWIgHgQQAbimAJiBQAIhiAQg9IAOgrQgCgjgJgoQgShOgjgUQgjgTg6gyIg0guQgujqgKhDQgHgugchLQgZhDgSggQgagtgNg6QgDgQAVg3IAdhHQAFgLgXgRIgXgQQg5AFgnAAQgzAAkdgcIotg5IhYADQiThGirhDQjQhSjShPQk9h3itg4Qizg5k6hNQj1g7jmgxQixgmkfgiQkSghjhgMQjPgLklAEQkNADj4APQjXAMj2AlQjKAfiqAmQh5AciIAqIhwAkIgLAQIgcgDQgDADhfAnQhtAskQB+QlLCaiQBTIrHGeIgugeIgoAbQhvAGgVAAQgZAAgwASQgVAIgmAHQgyAIhmALQlFAhisATQm3AykhAwQk7A0lFBXQkbBLhzA0QhxAyheBkQgvAygYApIBpA6Ig5BWQg/BjgeBAQgVAtgrBIQgiA6gSAwQgFAMgEAOIgpAnIgUBKQgTBbAHBZQAHBcAKAlQAFASAEAAIAsASIAHAWQAHAdABAlQAAA6AAADIgWAQQgWATAAASQgBASAIAHQAEAEADAAIgRAaIAAALQACAMAPAEQBQAMBWAOQCrAcBnAcQBqAcFnAwQCzAXCfASIAGhD");
	this.shape_30.setTransform(591.4831,166.8601);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.6,-0.6,1184.1999999999998,386.20000000000005);


// stage content:
(lib.bb1a1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_5
	this.instance = new lib.Tween1("synched",0);
	this.instance.setTransform(261,327.25);

	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.setTransform(261,327.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},35).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,rotation:-360},35).wait(1));

	// Layer_4
	this.instance_2 = new lib.bb1carai("synched",0);
	this.instance_2.setTransform(624.05,225.05,1,1,0,0,0,591.5,192.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({y:222.05},5).to({y:225.05},6).to({y:222.05},6).to({y:225.05},6).to({y:222.05},6).to({y:225.05},6).wait(1));

	// Layer_6
	this.instance_3 = new lib.bb1carai("synched",0);
	this.instance_3.setTransform(624.05,222.05,1,1,0,0,0,591.5,192.5);
	this.instance_3.alpha = 0.25;

	this.instance_4 = new lib.bb1carai("synched",0);
	this.instance_4.setTransform(624.05,225.05,1,1,0,0,0,591.5,192.5);
	this.instance_4.alpha = 0.25;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4},{t:this.instance_3}]}).wait(36));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(246.9,243.9,969.3000000000001,174.70000000000002);
// library properties:
lib.properties = {
	id: '77FD907C44E096408A2571A186318571',
	width: 430,
	height: 430,
	fps: 24,
	color: "#333333",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['77FD907C44E096408A2571A186318571'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused || stageChild.ignorePause){
			stageChild.syncStreamSounds();
		}
	}
}
an.handleFilterCache = function(event) {
	if(!event.paused){
		var target = event.target;
		if(target){
			if(target.filterCacheList){
				for(var index = 0; index < target.filterCacheList.length ; index++){
					var cacheInst = target.filterCacheList[index];
					if((cacheInst.startFrame <= target.currentFrame) && (target.currentFrame <= cacheInst.endFrame)){
						cacheInst.instance.cache(cacheInst.x, cacheInst.y, cacheInst.w, cacheInst.h);
					}
				}
			}
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;