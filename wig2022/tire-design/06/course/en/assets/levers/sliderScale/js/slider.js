	// Slider function Fuel Vs Tread
	function fuelVersusTread(val) {
		 document.getElementById('tread').value = 100 - val;
	}
	// Slider function Tread Vs Fuel	
	function treadVersusFuel(val) {
		document.getElementById('fuel').value = 100 - val;
	}
		// Slider function Fuel & Tread	
	function fuelTreadVersusLooseSnow(val) {
		document.getElementById('looseSnow').value = 100 - val;
	}
		// Slider function Loose & Snow	
	function looseSnowVersusFuelTread(val) {
		document.getElementById('fuelTread').value = 100 - val;
	}
		// Slider function Loose Ice Snow	
	function noiseVersusLooseIceSnow(val) {
		document.getElementById('looseIceSnow').value = 100 - val;
	}
		// Slider function Noise 	
	function looseIceSnowVersusNoise(val) {
		document.getElementById('tireNoise').value = 100 - val;
	}
			// Slider function Comfort	
	function handlingVersusComfort(val) {
		document.getElementById('comfort').value = 100 - val;
	}
		// Slider function Handling 	
	function comfortVersusHandling(val) {
		document.getElementById('handling').value = 100 - val;
	}
				// Slider function Comfort	
	function wetDryVersusAquaplaning(val) {
		document.getElementById('aquaplaning').value = 100 - val;
	}
		// Slider function Handling 	
	function aquaplaningVersusWetDry(val) {
		document.getElementById('wetDry').value = 100 - val;
	}
	// Color changes based on value of slider - Value color changes will go here 

	// Reset Slider Fuel VS Tread
	function resetSliderOne() {
		document.getElementById('fuel').value = "50";
		document.getElementById('tread').value = "50";
	}
	// Reset Slider Fuel Tread Vs Loose Snow
	function resetSliderTwo() {
		document.getElementById('fuelTread').value = "50";
		document.getElementById('looseSnow').value = "50";
	}
	// Reset Slider Loose Ice Snow VS Tire Noise
	function resetSliderThree() {
		document.getElementById('looseIceSnow').value = "50";
		document.getElementById('tireNoise').value = "50";
	}
	// Reset Slider Handling VS Comfort
	function resetSliderFour() {
		document.getElementById('handling').value = "50";
		document.getElementById('comfort').value = "50";
	}
	// Reset Slider Wet Dry VS Aquaplaning
	function resetSliderFive() {
		document.getElementById('wetDry').value = "50";
		document.getElementById('aquaplaning').value = "50";
	}
	

	// FOR ALL THE SLIDERS ON ONE DIV 
	// Slider function Fuel Vs Tread
	function fuelVersusTreadAll(val) {
		 document.getElementById('treadAll').value = 100 - val;
	}
	// Slider function Tread Vs Fuel	
	function treadVersusFuelAll(val) {
		document.getElementById('fuelAll').value = 100 - val;
	}
		// Slider function Fuel & Tread	
	function fuelTreadVersusLooseSnowAll(val) {
		document.getElementById('looseSnowAll').value = 100 - val;
	}
		// Slider function Loose & Snow	
	function looseSnowVersusFuelTreadAll(val) {
		document.getElementById('fuelTreadAll').value = 100 - val;
	}
		// Slider function Loose Ice Snow	
	function noiseVersusLooseIceSnowAll(val) {
		document.getElementById('looseIceSnowAll').value = 100 - val;
	}
		// Slider function Noise 	
	function looseIceSnowVersusNoiseAll(val) {
		document.getElementById('tireNoiseAll').value = 100 - val;
	}
			// Slider function Comfort	
	function handlingVersusComfortAll(val) {
		document.getElementById('comfortAll').value = 100 - val;
	}
		// Slider function Handling 	
	function comfortVersusHandlingAll(val) {
		document.getElementById('handlingAll').value = 100 - val;
	}
				// Slider function Comfort	
	function wetDryVersusAquaplaningAll(val) {
		document.getElementById('aquaplaningAll').value = 100 - val;
	}
		// Slider function Handling 	
	function aquaplaningVersusWetDryAll(val) {
		document.getElementById('wetDryAll').value = 100 - val;
	}

	// Reset Slider Reset All
	function resetSliderAll() {
		document.getElementById('fuelAll').value = "50";
		document.getElementById('treadAll').value = "50";
		document.getElementById('fuelTreadAll').value = "50";
		document.getElementById('looseSnowAll').value = "50";
		document.getElementById('looseIceSnowAll').value = "50";
		document.getElementById('tireNoiseAll').value = "50";
		document.getElementById('handlingAll').value = "50";
		document.getElementById('comfortAll').value = "50";
		document.getElementById('wetDryAll').value = "50";
		document.getElementById('aquaplaningAll').value = "50";
	}

